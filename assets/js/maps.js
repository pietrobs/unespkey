	var geocoder;
	var map;
	var marker;

	function initMap(){ 
		geocoder = new google.maps.Geocoder();
		var pp = {lat: -22.121586, lng: -51.406514};
		map = new google.maps.Map(document.getElementById('map'), {
			zoom: 17,
			center: pp
		});
		preencheMapa();
	}

	function preencheMapa(){
		var marker = new Array();
		var vUrl = "http://[::1]/unespapp/Problema/getJson";

		$.get(
			vUrl,
			function(response,status) { 
				if(status == "success") { 
					console.log(response);

					for (var i = 0; i < response.length; i++) {
						
						var latLng = new google.maps.LatLng(response[i].latitude, response[i].longitude);

						marker = new google.maps.Marker({
							position: latLng,
							map: map,
							animation: google.maps.Animation.DROP,
							descricao: response[i].descricao_problema,
							id: response[i].id_problema,
							tipo: response[i].tipo_problema,
							data: response[i].data_problema
						});	

						google.maps.event.addListener(marker, "click", (function(mrk, info) {
							return function() {
								openModal(mrk.id);
							};
						}(marker)));

					}
					map.setCenter(latLng);
				}else{
					console.log('Falha ao enviar ao servidor.'+response);
				}
			});


	}

	function openModal(id){
		var vUrl = "http://[::1]/unespapp/Problema/getJson/" + id;

		$.get(
			vUrl,
			function(response,status) { 
				if(status == "success") { 
					console.log(response);
					if(response.imagem != ""){
						var src = "data:image/png;base64," + response.imagem;
					}else{
						var src = "http://leeford.in/wp-content/uploads/2017/09/image-not-found.jpg";
					}
					$('#img').attr('src',src);
					$('#descricao').html("<strong>DESCRIÇÃO: </strong>" + response.descricao_problema);
					$('#myModal').modal('show');

				}else{
					console.log('Falha ao enviar ao servidor.'+response);
				}
			});
	}


	function codeAddress(endereco) {
		geocoder.geocode( { 'address': endereco}, function(results, status) {
			if (status == 'OK') {
				map.setZoom(16);
				map.setCenter(results[0].geometry.location);
				createMarker(results[0].geometry.location);
			} else {
				// alert('Geocode was not successful for the following reason: ' + status);
			}
		});
	}

	function createMarker(latLng) {
		if (marker) {
			marker.setMap(null);
			marker = "";
		}
		marker = new google.maps.Marker({
			position: latLng,
			draggable: true,
			map: map
		});
		marker.position = marker.getPosition();
		lat = marker.position.lat().toFixed(6);
		lng = marker.position.lng().toFixed(6);

		google.maps.event.addListener(marker, 'dragend', function() {
			marker.position = marker.getPosition();
			lat = marker.position.lat().toFixed(6);
			lng = marker.position.lng().toFixed(6);
			$("#latitude").val(lat);
			$("#longitude").val(lng);
		});

		$("#latitude").val(lat);
		$("#longitude").val(lng);
	}

	function getCoords(lat, lng) {
		$("#latitude").val(lat);
		$("#longitude").val(lng);
	}