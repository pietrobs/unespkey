<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_model extends CI_Model{
	private $table = "user";

	public function __construct(){
		parent::__construct();
	}

	public function login($data){
		$this->db->where($data);
		return $this->db->get($this->table)->row_array();
	}


	public function insert($dados){
		return $this->db->insert($this->table,$dados);
	}

	public function update($id_membro, $dados){
		$this->db->where('id_membro',$id_membro);
		return $this->db->update($this->table,$dados);
	}
}