<?php defined('BASEPATH') OR exit('No direct script access allowed');

// date_default_timezone_set('America/Sao_Paulo'); 

function getMensagem($data){
	$html = "";
	if ($data->flashdata('sucesso')) {

		$html ='<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong>Sucesso!</strong> '.$data->flashdata('sucesso').'
		</div>';

	}else if ($data->flashdata('erro')) { 
		$html ='<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong>Ops!</strong> '.$data->flashdata('erro').'
		</div>';
	}
	echo $html;	
}

function dia($data){
	if($data == null) return '-';
	$date = new DateTime($data);
	return $date->format('d/m/Y');
}

function hora($data){
	if($data == null) return '-';
	$date = new DateTime($data);
	return $date->format('H:i');
}

function semana($data){
	if($data == null) return '-';
	$date = new DateTime($data);
	return $date->format('W');
}

function permanencia($data_1, $data_2){
	$data1 = new DateTime($data_1);
	$data2 = new DateTime($data_2);

	$diferenca = $data1->diff($data2);

	return $diferenca->format('%H:%I');
}

function dia_da_semana($data){
	if($data == null) return '-';
	$data = date('w',strtotime($data));

	switch ($data) {
		case '0':
		$dia = 'Domingo';
		break;
		case '1':
		$dia = 'Segunda-Feira';
		break;
		case '2':
		$dia = 'Terça-Feira';
		break;
		case '3':
		$dia = 'Quarta-Feira';
		break;
		case '4':
		$dia = 'Quinta-Feira';
		break;
		case '5':
		$dia = 'Sexta-Feira';
		break;
		case '6':
		$dia = 'Sábado';
		break;
		
	}
	return $dia;
}