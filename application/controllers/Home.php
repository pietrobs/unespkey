<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('usuario_model','modelusuario');
	}

	public function index(){
		$this->load->view('admin/login');
	}

	public function logout(){
		session_destroy();
		redirect('Painel');
	}

	public function logar(){
		$data_login['login_user'] = $this->input->post('login');
		$data_login['password_user'] = md5($this->input->post('senha'));


		$usuario = $this->modelusuario->login($data_login);

		if(count($usuario) == 0){
			$this->session->set_flashdata('erro','Login e/ou senha inválido(s)');
			redirect(base_url());
		}else{
	
			$this->session->set_userdata('usuario',$usuario);
			redirect('Painel');
		}
	}





}
