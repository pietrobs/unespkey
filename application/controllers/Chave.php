<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once dirname(__FILE__) . "\Response.php";

class Chave extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('chave_model','model');

		if(!isset($this->session->usuario)){
			//redirect(base_url());
			$response = new Response();
			$response->set_code(Response::UNAUTHORIZED);
			$response->set_data([
				'erro' => 'Sem permissão.'
			]);

		}

		date_default_timezone_set('America/Sao_Paulo');
	}

	public function index(){
		//criar a view
	}

	public function create(){
		
		$id_chave = $this->input->post('id_chave');
		$exists = $this->model->get(['id_chave' => $id_chave]);

		$response = new Response();

		if(!$exists){
			$this->model->insert(['id_chave' => $id_chave]);
			$response->set_code(Response::SUCCESS);
			
		}
		else{
			$response->set_code(Response::BAD_REQUEST);
			$response->set_data([
				'erro' => 'Chave já existente!'
			]);

		}
		$response->send();
	}

	/**
		Lista todas as chaves, com o usuário passando por post em 'texto' a TAG que está procurando ou o ID da chave.
		**/
		public function list(){ 
			$response = new Response();
			
			$termos_de_busca = $this->input->post('texto');

			$chaves = $this->model->listar_chaves_com_estes($termos_de_busca);

			$response->set_code(Response::SUCCESS);
			$response->set_data($chaves);

			$response->send();
		}

		public function list_emprestadas(){
			$response = new Response();


		//chaves que estão emprestadas até o momento:
			$chaves_emprestadas = $this->model->listar_emprestadas();

			$response->set_code(Response::SUCCESS);
			$response->set_data($chaves_emprestadas);



			$response->send();

		}

		public function emprestar_chave(){
			$this->load->model('historico_model');


			$data['id_chave'] = $this->input->post('id_chave');
			$data['id_pessoa'] =  $this->input->post('id_pessoa');

			//verificar se a pessoa possui permissão para pegar a chave:

			$response = new Response();

		//verificar se já não está emprestada:
			$resposta = $this->historico_model->get(['id_chave' => $data['id_chave'], 'historico_pessoa_chave.id_pessoa' => $data['id_pessoa'], 'data_devolucao' => NULL]);
		if($resposta == false){ //quer dizer que ninguém emprestou essa chave ainda
			$result = $this->model->emprestar_chave($data);
			if($result){

				$response->set_code(Response::SUCCESS);

			}else{
				$response->set_code(Response::DB_ERROR_INSERT);
				$response->set_data([
					'erro' => 'Erro ao inserir no banco de dados.'
				]);
			}
			
		}
		else{ //se não já tem algue´m que emprestou:
			$response->set_code(Response::BAD_REQUEST);
			$response->set_data([
				'erro' => 'Alguém já emprestou essa chave.'
			]);
		}

		$response->send();
		
	}

	public function devolver(){
		$this->load->model('historico_model');

		// $data['id_chave'] = $id_chave;
		// $data['id_pessoa'] = $id_pessoa;

		$data['id_chave'] = $this->input->post('id_chave');
		//$data['id_pessoa'] =  $this->input->post('id_pessoa');

		

		$response = new Response();

		//verificar se a chave tá emprestada:
		$resposta = $this->historico_model->get(['id_chave' => $data['id_chave'], 'data_devolucao' => NULL]);
		if($resposta == false){ //quer dizer que a chave não está emprestada:
			$response->set_code(Response::BAD_REQUEST);
			$response->set_data([
				'erro' => 'Esta chave não está emprestada.'
			]);
		}
		else{ // a chave está emprestada:
			$resposta_update = $this->historico_model->update(['data_devolucao' => date("Y/m/d H:i:s")], ['id_chave' => $data['id_chave']]);

			if($resposta_update == false){
				$response->set_code(Response::DB_ERROR_UPDATE);
				$response->set_data([
					'erro' => 'Erro ao devolver a chave.'
				]);
			}
			else{ //se deu certo:
				$response->set_code(Response::SUCCESS);
			}
		}

		$response->send();

	}

	public function create_tag(){
		// $data['tag_name'] = $tag_name;
		// $data['id_chave'] = $id_chave;

		$data['tag_name'] = $this->input->post('tag_name');
		$data['id_chave'] = $this->input->post('id_chave');

		$response = new Response();

		$exists = $this->model->get_tag(['tag_name' => $data['tag_name'], 'id_chave' => $data['id_chave']]);
		
		if($exists == false){
			$resposta = $this->model->insert_tag($data);
			if($resposta){
				$response->set_code(Response::SUCCESS);
			}
			else{
				$response->set_code(Response::DB_ERROR_INSERT);
					$response->set_data([
						'erro' => 'Erro ao inserir no banco de dados.'
					]);
			}
				
		}
		else{ 
			$response->set_code(Response::BAD_REQUEST);
					$response->set_data([
						'erro' => 'Erro ao criar tag. Já existe essa tag para esta chave.'
					]);
		}

		$response->send();

	}


	

	//UPDATE

	//DELETE



}
