<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('usuario_model','modelusuario');
	}

	public function index(){
		$this->load->view('admin/login');
	}

	public function criar($id_membro){
		if(!isset($this->session->usuario)){
			redirect(base_url());
		}
		if($this->session->usuario['id_nivel'] != 2){
			redirect(base_url('Painel'));
		}

		$dados['login'] = $this->input->post('login');
		$dados['senha'] = md5($this->input->post('senha'));
		$dados['id_nivel'] = $this->input->post('id_nivel');
		$dados['id_membro'] = $id_membro;

		if($this->modelusuario->insert($dados)){
			$this->session->set_flashdata('sucesso','Usuário criado com sucesso');
		}else{
			$this->session->set_flashdata('erro','Houve um erro ao criar o usuário.');
		}
		redirect(base_url('Membro/editar/'.$id_membro));

	}

	public function alterar($id_membro){
		if(!isset($this->session->usuario)){
			redirect(base_url());
		}
		if($this->session->usuario['id_nivel'] == 1){
			redirect(base_url('Painel'));
		}

		

		if($this->input->post('id_nivel') != NULL){
			$dados['id_nivel'] = $this->input->post('id_nivel');
		}
		if($this->input->post('senha') != NULL){
			$dados['senha'] = md5($this->input->post('senha'));
		}

		if($this->modelusuario->update($id_membro,$dados)){
			$this->session->set_flashdata('sucesso','Senha alterada.');
		}else{
			$this->session->set_flashdata('erro','Houve um erro ao alterar a senha.');
		}
		redirect(base_url('Membro/editar/'.$id_membro));

	}

	public function alterar_foto($id){
		
		$configuracao = array(
			'upload_path'   => './assets/uploads/',
			'allowed_types' => 'jpg',
			'file_name'     =>  $id,
			'max_size'      => '5000'
		);      
		
		$this->load->library('upload');
		$this->upload->initialize($configuracao);


		// Se a pessoa já possui uma foto de perfil, essa foto será excluida.
		if(file_exists('./assets/uploads/'.$id.'.jpg')){
			unlink('./assets/uploads/'.$id.'.jpg');
			echo "Foto antiga apagada";		
		}

		if (!$this->upload->do_upload()){
			$this->session->set_flashdata('erro', 'Erro ao alterar imagem.');
			echo $this->upload->display_errors();
		}
		$this->session->set_flashdata('sucesso', 'Sua foto foi alterada! Talvez seja necessário limpar o cache do navegador <strong>(CTRL + F5)</strong> no computador, no celular é mais complicado.');
		redirect('Painel');
	}



}
