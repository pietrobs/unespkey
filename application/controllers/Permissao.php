<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once dirname(__FILE__) . "\Response.php";

class Permissao extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('permissao_model','model');

		if(!isset($this->session->usuario)){
			//redirect(base_url());
			$response = new Response();
			$response->set_code(Response::UNAUTHORIZED);
			$response->set_data([
				'erro' => 'Sem permissão.'
			]);
		}

		date_default_timezone_set('America/Sao_Paulo');
	}

	public function index(){
		//criar a view
	}

	public function check_permission(){
		$response = new Response();

		$data['id_pessoa'] = $this->input->post('id_pessoa');
		$data['id_chave'] = $this->input->post('id_chave');

		$resposta = $this->model->get(['id_pessoa' => $data['id_pessoa'], 'id_chave' => $data['id_chave']]);

		if($resposta){
			$response->set_code(Response::BAD_REQUEST);
			$response->set_data(['erro' => 'Usuário não possui permissão para pegar a chave.']);
		}
		else{
			$response->set_code(Response::SUCCESS);

		}

		$response->send();
	}

		public function list_all(){
		$response = new Response();

		$resposta = $this->model->get();
	
		$response->set_code(Response::SUCCESS);
		$response->set_data($resposta);


		$response->send();
	}

	public function list_all_permissions_of_some_person(){
		$response = new Response();

		$id_pessoa = $this->input->post('id_pessoa');

		$resposta = $this->model->get(['id_pessoa' => $id_pessoa]);
	
		$response->set_code(Response::SUCCESS);
		$response->set_data($resposta);


		$response->send();
	}

	public function list_people_who_has_permission_to_get_this_key(){
		$response = new Response();

		$id_chave = $this->input->post('id_chave');

		$resposta = $this->model->get(['id_chave' => $id_chave]);

		if($resposta){
			$response->set_code(Response::SUCCESS);
			$response->set_data($resposta);
		}
		else{
			$response->set_code(Response::DB_ERROR_INSERT);
				$response->set_data([
					'erro' => 'Erro ao consultar o banco de dados.'
				]);
		}

		$response->send();
	}	

	public function create(){
		$response = new Response();

		$data['id_pessoa'] = $this->input->post('id_pessoa');
		$data['id_chave'] = $this->input->post('id_chave');

		$exists = $this->model->get(['id_pessoa' => $data['id_pessoa'], 'id_chave' => $data['id_chave']]);

		if(!$exists){
			$resposta = $this->model->insert(['id_pessoa' => $data['id_pessoa'], 'id_chave' => $data['id_chave']]);
			if($resposta){
				$response->set_code(Response::SUCCESS);
				
			}
			else{
				$response->set_code(Response::DB_ERROR_INSERT);
					$response->set_data([
						'erro' => 'Erro ao inserir no banco de dados.'
					]);

			}
			
		}
		else{ //se já existe:
			$response->set_code(Response::BAD_REQUEST);
					$response->set_data([
						'erro' => 'Essa pessoa já possui essa permissão para esta chave.'
					]);
		}


		$response->send();
	}

	public function delete(){
		$response = new Response();

		$data['id_pessoa'] = $this->input->post('id_pessoa');
		$data['id_chave'] = $this->input->post('id_chave');


		$exists = $this->model->get(['id_pessoa' => $data['id_pessoa'], 'id_chave' => $data['id_chave']]);

		if($exists){

			$resposta = $this->model->delete(['id_pessoa' => $data['id_pessoa'], 'id_chave' => $data['id_chave']]);
			if($resposta){
				$response->set_code(Response::SUCCESS);
				
			}
			else{
				$response->set_code(Response::DB_ERROR_DELETE);
					$response->set_data([
						'erro' => 'Erro ao deletar do banco de dados.'
					]);

			}
		}
		else{ //se nao existe:
			$response->set_code(Response::BAD_REQUEST);
					$response->set_data([
						'erro' => 'Permissão não existente.'
					]);
		}


		$response->send();
	}




	

	//UPDATE




}
