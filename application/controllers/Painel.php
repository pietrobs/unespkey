<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Painel extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if(!isset($this->session->usuario)){
			redirect(base_url());
		}

		date_default_timezone_set('America/Sao_Paulo');
	}

	public function index(){
		$this->load->view('admin/importacoes');
		$this->load->view('admin/topbar');
		$this->load->view('admin/sidebar');
		$this->load->view('admin/inicio');
		$this->load->view('admin/footer');
	}

	public function chave(){
		$this->load->view('admin/importacoes');
		$this->load->view('admin/topbar');
		$this->load->view('admin/sidebar');
		$this->load->view('admin/chave');
		$this->load->view('admin/footer');
	}

	public function pessoa(){
		$this->load->view('admin/importacoes');
		$this->load->view('admin/topbar');
		$this->load->view('admin/sidebar');
		$this->load->view('admin/pessoa');
		$this->load->view('admin/footer');
	}

	public function sair(){
		session_destroy();
		redirect(base_url());
	}

}
