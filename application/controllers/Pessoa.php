<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once dirname(__FILE__) . "\Response.php";

class Pessoa extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('pessoa_model','model');

		if(!isset($this->session->usuario)){
			//redirect(base_url());
			$response = new Response();
			$response->set_code(Response::UNAUTHORIZED);
			$response->set_data([
				'erro' => 'Sem permissão.'
			]);
		}

		date_default_timezone_set('America/Sao_Paulo');
	}

	public function index(){
		//criar a view
	}

	public function list(){
		$response = new Response();

		$pessoas = $this->model->get();

		$response->set_code(Response::SUCCESS);
		$response->set_data([$pessoas]);
	
		$response->send();
	}

	public function create(){
		$response = new Response();

		$nome = $this->input->post('nome');

		$resposta = $this->model->insert(['nome' => $nome]);
		if($resposta){
			$response->set_code(Response::SUCCESS);
			
		}
		else{
			$response->set_code(Response::DB_ERROR_INSERT);
				$response->set_data([
					'erro' => 'Erro ao inserir no banco de dados.'
				]);

		}

		$response->send();
	}


	

	//UPDATE

	//DELETE



}
