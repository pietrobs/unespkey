<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>UNESPapp</title>
  <link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/css/datepicker3.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/css/styles.css') ?>" rel="stylesheet">
  <link rel="icon" type="imagem/png" href="<?= base_url('assets/img/logo-dvora.png');?>" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
  <!--Custom Font-->
<!--   <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet"> -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
<![endif]-->

<script src="<?= base_url('assets/js/jquery.js') ?>"></script>
<script src="<?= base_url('assets/js/maps.js') ?>"></script>

<style>
    .ui-autocomplete { z-index:2147483647 !important; }
</style>
</head>
<body>

<!--   <div id="preloader">
    <div class="inner">
      <div class="bolas">
        <div></div>
        <div></div>
        <div></div>                    
      </div>
    </div>
  </div>
  <script>
    $(window).on('load', function () {
      $('#preloader .inner').delay(1000).fadeOut();
      $('#preloader').delay(350).fadeOut('slow'); 
      $('body').delay(350).css({'overflow': 'visible'});
    })
  </script> -->
  <!-- <script type="text/javascript" src="<?= base_url('assets/js/jquery.backstretch.min.js') ?>"></script> -->
  <!-- <script>
    $.backstretch("<?= base_url('assets/img/login-bg.jpg') ?>", {speed: 500});
  </script> -->