<div class="row">
	<ol class="breadcrumb">
		<li>
			<a href="<?= base_url('Painel') ?>">
				<em class="fa fa-home">&nbsp</em>Início
			</a>
		</li>
	</ol>
</div><!--/.row-->


<div class="col-md-4 mt">
<div class="panel panel-primary ">
		<div class="panel-body">
		<h1>Buscar</h1>
			<div class="form-group">
				<div class="input-group">
				<div class="input-group-addon"><i class="fas fa-search"></i></div>
				<input type="text" class="form-control" id="id_chave" placeholder="Identificador da chave">
				</div>
			</div>
			<button type="submit" class="btn btn-theme" id="buscar">Buscar</button>
		</div>
	</div>

	<div class="loading" align="center">
		<img width="150" src="<?= base_url('assets/img/loading.gif') ?>" alt="loading">
	</div>

	<div class="panel panel-primary" id="panel_key">
		<div class="panel-body">
		<h1><i class="fas fa-key"></i> Encontrada</h1>
			<div class="form-group">
				<p>Identificador: <span id="id_key">55A</span></p>
			</div>
			<button type="submit" class="btn btn-theme" id="emprestar">Emprestar</button>
		</div>
	</div>

	<div class="panel panel-primary" id="not_found">
		<div class="panel-body">
		<h1><i class="fas fa-key"></i> Não Encontrada</h1>
			<div class="form-group">
				<p>Tag: <span id="tag_key">CaCiC</span></p>
			</div>
			<button type="submit" class="btn btn-theme" id="criar_tag">Criar TAG</button>
			<button type="submit" class="btn btn-theme" id="criar_chave">Criar chave</button>
		</div>
	</div>
</div>

<div class="col-md-8 mt">
	<div class="panel panel-primary ">
		<div class="panel-body">
		<h1>Emprestadas</h1>
			<table class="table table-hover">
			<thead>
			<tr>
				<th>#</th>
				<th>Identificação</th>
				<th>Emprestada por</th>
				<th>Hora</th>
				<th>Ação</th>
			</tr>
			</thead>
				<tbody>
				<tr>
						<td><i class="fas fa-key"></i></td>
						<td>55B</td>
						<td>Pietro Barcarollo Schiavinato</td>
						<td>16:22 - 17/08/2018</td>
						<td><button class="btn btn-theme devolver" value="55B">Devolver</button></td>
					</tr>
					<tr>
						<td><i class="fas fa-key"></i></td>
						<td>55C</td>
						<td>Darlan Murilo Nakamura</td>
						<td>13:22 - 17/08/2018</td>
						<td><button class="btn btn-theme devolver" value="55C">Devolver</button></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div id="m_emprestar" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Emprestar chave</h4>
      </div>
      <div class="modal-body">
        <label for="selecione">Selecione a pessoa</label>
		<select name="selecione" id="pessoas" class="form-control">
			<option value="#">Darlan</option>
			<option value="#">Pietro</option>
		</select>
		<div id="outra_pessoa">
			<label>Nome da pessoa</label>
			<input type="text" class="form-control">
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-theme">Emprestar</button>
        <button type="button" id="nova_pessoa" class="btn btn-primary">Outra pessoa</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="m_tag" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Criar TAG</h4>
      </div>
      <div class="modal-body">
		<div class="row">
		<div class="col-md-12">
			<label>Chave</label>
			<input type="text" class="form-control">
			<button class="btn btn-primary" id="buscar_chave">Buscar</button>
		</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-theme">Criar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="m_chave" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Criar Chave</h4>
      </div>
      <div class="modal-body">
	  <div class="row">
		<div class="col-md-4">
			<label>Identificador</label>
			<input type="text" class="form-control">
		</div>
	  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-theme">Criar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

	const url = "URL_DO_WS";

	$(document).ready(() => {
		$('#panel_key').hide();
		$('.loading').hide();
	});

	$('#emprestar').click(() => {
		$('#m_emprestar').modal('show');
	});

	$('.devolver').click(()=> {
		console.log(this.value);
		_return();
	});

	$('#buscar').click(() => {
		_search();
	});

	$('#nova_pessoa').click(() => {
		$("#outra_pessoa").fadeIn(500);
	});

	$('#criar_tag').click(() => {
		$('#m_tag').modal('show');
	});

	$('#criar_chave').click(() => {
		$('#m_chave').modal('show');
	});

	_fillSelect = (array) => {
		let html = "";
		array.map(option => {
			html += "<option value='" + option.id_pessoa + "'>" + nome + "</option>";
		})
		$('#pessoas').remove();
		$('#pessoas').html(html);
	}

	_fillKeyPanel = (key) => {
		$("#id_key").html(key.id_chave);
	}


	_fillTable = () => {
		// $.post(url + '/fillTable',data).done((response) =>{

		// });
	}

	_search = () => {
		let data = 
		{
			id_chave: $('#id_chave').val()
		}
		$('.loading').show();
		setTimeout(() => {
			$('.loading').hide();
			$('#outra_pessoa').hide();
			$('#nova_pessoa').val("");
			$('#panel_key').fadeIn(1000);
			
		}, 500);
		// $.post(url + '/search',data).done((response) =>{

		// });
	}

	_get = () => {
		let data = 
		{
			id_chave: $('#id_key').html(),
			id_pessoa: $('#pessoas').val()
		}

		console.log(data);

		// $.post(url + '/emprestar', data).done((response) =>){

		// });
	}

	_return = (id) => {
		let data = 
		{
			id_chave: id,
		}

		console.log("Devolvendo chave: " + id);

		// $.post(url + '/devolver', data).done((response) =>){

		// });
	}

</script>

