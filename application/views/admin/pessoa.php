<div class="row">
	<ol class="breadcrumb">
		<li>
			<a href="<?= base_url('Painel') ?>">
				<em class="fas fa-user">&nbsp</em>Pessoa
			</a>
		</li>
	</ol>
</div><!--/.row-->


<div class="col-md-4 mt">
<div class="panel panel-primary ">
		<div class="panel-body">
		<h1>Buscar</h1>
			<div class="form-group">
				<div class="input-group">
				<div class="input-group-addon"><i class="fas fa-search"></i></div>
				<input type="text" class="form-control" id="nome_pessoa" placeholder="Nome">
				</div>
			</div>
			<button type="submit" class="btn btn-theme" id="buscar">Buscar</button>
		</div>
	</div>

	<div class="loading hide" align="center">
		<img width="150" src="<?= base_url('assets/img/loading.gif') ?>" alt="loading">
	</div>

	<div class="panel panel-primary" id="panel_key">
		<div class="panel-body">
		<h1><i class="fas fa-user"></i> Encontrada</h1>
        <ul>
        <li>
            <a type="submit" class="ver" value="1">Pietro Barcarollo Schiavinato</a>
        </li>
        <br>
        </ul>
		</div>
	</div>
</div>

<div class="col-md-8 mt">
	<div class="panel panel-primary ">
		<div class="panel-body">
		<h1>Pessoas</h1>
			<table class="table table-hover">
			<thead>
			<tr>
				<th>#</th>
				<th>Nome</th>
				<th>Ação</th>
			</tr>
			</thead>
				<tbody>
				<tr>
						<td><i class="fas fa-user"></i></td>
						<td>Pietro Barcarollo Schiavinato</td>
						<td><button class="btn btn-theme ver" value="55B">Ver histórico</button></td>
					</tr>
					<tr>
						<td><i class="fas fa-user"></i></td>
						<td>Darlan Murilo Nakamura</td>
						<td><button class="btn btn-theme ver" value="55C">Ver histórico</button></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>





