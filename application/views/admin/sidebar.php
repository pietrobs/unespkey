<?php 

if(file_exists('./assets/uploads/'.$this->session->usuario['id_user'].'.jpg')){
  $foto = $this->session->usuario['id_user'].".jpg";
}else{
  $foto = "default.png";
}



?>

<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
  <div class="profile-sidebar">
    <div class="profile-userpic">
      <img src="<?= base_url('assets/uploads/'.$foto); ?>" class="img-responsive" alt="Profile User Pic" data-toggle="modal" data-target="#myModalFoto">
    </div>
    <div class="profile-usertitle">
      <div class="profile-usertitle-name"><?= $this->session->usuario['login_user'] ?></div>
      <div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
    </div>
    <div class="clear"></div>
  </div>
  <div class="divider"></div>
<!--    <form role="search">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Search">
      </div>
    </form> -->
    <ul class="nav menu">



      <li class="mt">
        <a href="<?= base_url('Painel') ?>">
          <i class="fa fa-home"></i>
          <span> Início</span>
        </a>
      </li>

      <li>
        <a href="<?= base_url('Painel/chave') ?>">
          <i class="fas fa-key"></i>
          <span> Chave</span>
        </a>
      </li>


      <li>
        <a href="<?= base_url('Painel/pessoa') ?>">
          <i class="fas fa-user"></i>
          <span> Pessoa</span>
        </a>
      </li>
    </ul>
  </div>
  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">



