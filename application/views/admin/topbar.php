<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    <div class="container-fluid bg-theme">
        <div class="navbar-header ">
            <a type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="navbar-brand" href="<?= base_url('Painel') ?>"><span>UNESP</span>key</a>
            <ul class="nav navbar-top-links navbar-right">

               <li class="dropdown">
                <!-- <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#"> -->
                    <!-- <a class="dropdown-toggle count-info" href="<?= base_url('Membro/coffee') ?>">
                     <i class="fas fa-coffee"></i>
                     <span class="label label-info"></span>
                 </a> -->

<!--                 <ul class="dropdown-menu dropdown-alerts">

                    <li>
                        <a href="#">
                            <div>
                                <em class="fa fa-envelope"></em> Seja bem vindo!
                                <span class="pull-right text-muted small">0 mins ago</span>
                            </div>
                        </a>

                    </li>


                </ul> -->
            </li>
        </ul>
    </div>
</div><!-- /.container-fluid -->
</nav>