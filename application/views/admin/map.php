<style>
#map {
	width: 100%;
	height: 400px;
	background-color: grey;
}

</style>

<div class="row">
	<ol class="breadcrumb">
		<li>
			<a href="<?= base_url('Painel') ?>">
				<em class="fa fa-home">&nbsp</em>Problemas
			</a>
		</li>
	</ol>
</div><!--/.row-->

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Problemas</h1>
		<?php getMensagem($this->session) ?>
	</div>
</div><!--/.row-->



<div class="col-md-12 mt">
	<div class="panel panel-primary ">
		<div class="panel-body">
			<div id="map"></div>
		</div>
	</div>
</div>



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="modal-title">Ver problema</h4>
			</div>
			<div class="modal-body" id="modal-body">
				<img id="img" src="" class="img-responsive"/>
				<br>
				<p id="descricao"></p>			
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>

<script async defer src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCEuLyG49U4wIMuISmJI2n1vLsPtACUC0w&callback=initMap'></script>

