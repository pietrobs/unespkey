<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Dashboard">
	<meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<link rel="icon" type="imagem/png" href="<?= base_url('assets/img/logo-dvora.png');?>" />

	<title>UNESPapp</title>

	<!-- Bootstrap core CSS -->
	<link href="<?= base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet">
	<!--external css-->
	<link href="<?= base_url('assets/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />

	<!-- Custom styles for this template -->
	<link href="<?= base_url('assets/css/style.css'); ?>" rel="stylesheet">
	<link href="<?= base_url('assets/css/style-responsive.css'); ?>" rel="stylesheet">

</head>

<body>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

      <div id="login-page">
      	<div class="container">

      		<form class="form-login" action="<?= base_url('Home/logar') ?>" method="POST" style="background-color: rgba(255,255,255,.3)">
      			<h2 class="form-login-heading">entrar</h2>
      			<div class="login-wrap">
      				<input type="text" name="login" class="form-control" placeholder="Login" autocomplete="false">
      				<br>
      				<input type="password" name="senha" class="form-control" placeholder="Senha">
      				<label class="checkbox">
      					<span class="pull-right">
      						<!-- <a data-toggle="modal" href="login.html#myModal"> Esqueceu sua senha?</a> -->

      					</span>
      				</label>
      				<button class="btn btn-theme btn-block" href="index.html" type="submit"><i class="fa fa-lock"></i> ENTRAR</button>
      				<?php if($this->session->flashdata('erro')){ ?>
      				<br>
      				<div class="alert alert-danger alert-dismissible" role="alert">
      					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      					<strong>ERRO DE AUTENTICAÇÃO!</strong> Verifique se o login e/ou senha foram digitados corretamente.
      				</div>
      				<?php } ?>
		        </div>

		        <<!--></!--> Modal -->
		        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
		        	<div class="modal-dialog">
		        		<div class="modal-content">
		        			<div class="modal-header">
		        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        				<h4 class="modal-title">Forgot Password ?</h4>
		        			</div>
		        			<div class="modal-body">
		        				<p>Enter your e-mail address below to reset your password.</p>
		        				<input type="text" name="email2" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

		        			</div>
		        			<div class="modal-footer">
		        				<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
		        				<button class="btn btn-theme" type="button">Submit</button>
		        			</div>
		        		</div>
		        	</div>
		        </div>
		        <!-- modal -->

		    </form>	  	

		</div>
	</div>

	<script src="<?= base_url('assets/js/jquery.js') ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>

	<!--BACKSTRETCH-->
	<script type="text/javascript" src="<?= base_url('assets/js/jquery.backstretch.min.js') ?>"></script>
	<script>
		$.backstretch("<?= base_url('assets/img/login-bg.jpg') ?>", {speed: 500});
	</script>



</body>
</html>
