<div class="row">
	<ol class="breadcrumb">
		<li>
			<a href="<?= base_url('Painel') ?>">
				<em class="fas fa-key">&nbsp</em>Chave
			</a>
		</li>
	</ol>
</div><!--/.row-->


<div class="col-md-12">
    <a href="#" id="cadastrar" class="btn btn-lg btn-theme"><i class="fas fa-plus"></i> Cadastrar chave</a>
</div>


<div class="col-md-4 mt">
<div class="panel panel-primary ">
		<div class="panel-body">
		<h1>Buscar</h1>
			<div class="form-group">
				<div class="input-group">
				<div class="input-group-addon"><i class="fas fa-search"></i></div>
				<input type="text" class="form-control" id="id_chave" placeholder="Identificador da chave">
				</div>
			</div>
			<button type="submit" class="btn btn-theme" id="buscar">Buscar</button>
		</div>
	</div>
</div>

<div class="col-md-8 mt">
	<div class="panel panel-primary ">
		<div class="panel-body">
		<h1>Chaves</h1>
			<table class="table table-hover">
			<thead>
			<tr>
				<th>#</th>
				<th>Identificação</th>
				<th>Ação</th>
			</tr>
			</thead>
				<tbody>
				<tr>
						<td><i class="fas fa-key"></i></td>
						<td>55B</td>
						<td><button class="btn btn-theme ver" value="55B">Ver histórico</button></td>
					</tr>
					<tr>
						<td><i class="fas fa-key"></i></td>
						<td>55C</td>
						<td><button class="btn btn-theme ver" value="55C">Ver histórico</button></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>


<div id="m_cadastrar" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Cadastrar chave</h4>
      </div>
      <div class="modal-body">
      <div class="row">
      <div class="col-md-4">
        <label for="identificador">Identificador da chave</label>
        <input type="text" id="identificador" name="identificador" class="form-control">
      </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-theme">Cadastrar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<script>
    $('#cadastrar').click(() => {
        $('#m_cadastrar').modal('show');
    })
</script>
